// Copyright (c) 2014, Anders Holmgren. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

/// The shelf_oauth library.
///
/// This is an awesome library. More dartdocs go here.
library shelf_oauth.token;

export 'src/token.dart';
