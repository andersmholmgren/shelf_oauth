// Copyright (c) 2014, The shelf_oauth authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_oauth.token;

import 'preconditions.dart';

abstract class OAuthToken {
  /// The token
  final String token;

  /// Constructs a new token
  OAuthToken(this.token) {
    ensure(token, isNotNull);
  }

  factory OAuthToken.fromJson(Map json) {
    final type = json['type'];
    switch (type) {
      case OAuth1Token._type:
        return new OAuth1Token.fromJson(json);
      case OAuth2Token._type:
        return new OAuth2Token.fromJson(json);
      default:
        throw new ArgumentError('missing or invalid type in json');
    }
  }

  Map _toJson(String type) => {'type': type, 'token': token};

  Map toJson();

  String toString() => 'OAuthToken(${toJson()})';
}

class OAuth1Token extends OAuthToken {
  static const String _type = 'oauth1';

  /// The token secret
  final String secret;

  OAuth1Token(String token, this.secret) : super(token) {
    ensure(secret, isNotNull);
  }

  OAuth1Token.fromJson(Map json) : this(json['token'], json['secret']);

  Map toJson() => super._toJson(_type)..addAll({'secret': secret});
}

class OAuth2Token extends OAuthToken {
  static const String _type = 'oauth2';

  OAuth2Token(String token) : super(token);

  OAuth2Token.fromJson(Map json) : this(json['token']);

  Map toJson() => super._toJson(_type);
}
